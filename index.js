const {Datastore} = require ('@google-cloud/datastore')
const {PubSub} = require("@google-cloud/pubsub");
const cors = require ('cors')
const express = require ('express')
var differenceInHours = require('date-fns/differenceInHours')
const {writeFile} = require('fs').promises;
const pubsub = new PubSub({projectId: "t2-issue-review-board"});

const app = express();
app.use(cors())
app.use(express.json())

const datastore = new Datastore();

async function pullIssues()
{
    const subscription = pubsub.subscription("issue-subscription"); // pointing to the subscription resource we defined in Terraform
    subscription.on("message", async (message) => 
    {
        const newIssue = JSON.parse(message.data); // parses the JSON message into a JS object! 
        const issueKey = datastore.key("issue"); // if you don't have a Kind, it will create a Kind (in this case Kind "Issue")

        const entity = // we create the entity to put into datastore
        {
            key: issueKey,
            data: newIssue
        };

        await datastore.insert(entity); // puts the new entity within datastore under the key defined
        message.ack();  // subscriber acknowledging that it received the message from the Topic so it gets removed from the Topic
    });
}
pullIssues();


app.get('/issues', async (req, res) => {

    if(req.query.type){
        const query = datastore.createQuery('issue').filter('type','=',req.query.type);
        const result = await datastore.runQuery(query);
        res.status(200).send(result);
        
    } else if(req.query.dateposted){
        const query = datastore.createQuery('issue').filter('dateposted','=',req.query.dateposted);
        const result = await datastore.runQuery(query);
        res.status(200).send(result);

    } else if(req.query.dateofissue){
        const query = datastore.createQuery('issue').filter('dateofissue','=',req.query.dateofissue);
        const result = await datastore.runQuery(query);
        res.status(200).send(result);
        
    } else if(req.query.reviewed){
        const query = datastore.createQuery('issue').filter('reviewed','=',req.query.reviewed);
        const result = await datastore.runQuery(query);
        res.status(200).send(result);

    } else if(req.query.highlighted){
        const query = datastore.createQuery('issue').filter('highlighted','=',req.query.highlighted);
        const result = await datastore.runQuery(query);
        res.status(200).send(result);
        
    } else {
        const query = datastore.createQuery('issue');
        const result = await datastore.runQuery(query);
        res.status(200).send(result);
    }
})

app.patch('/issues/reviewed', async (req, res) =>{
    const query = datastore.createQuery('issue').filter('type','=',req.body.type).filter('dateposted','=',req.body.dateposted).filter('dateofissue','=',req.body.dateofissue).filter('reviewed','=',req.body.reviewed).filter('highlighted','=',req.body.highlighted).filter('description','=',req.body.description);
    await datastore.runQuery(query, function(err, entities) {
        entities.forEach(f => {
            f.reviewed = true;
            datastore.save(f);
        })
        });
    res.status(200).send("Marked issue as reviewed");
});

app.patch('/issues/highlight', async (req, res) =>{
    const query = datastore.createQuery('issue').filter('type','=',req.body.type).filter('dateposted','=',req.body.dateposted).filter('dateofissue','=',req.body.dateofissue).filter('reviewed','=',req.body.reviewed).filter('highlighted','=',req.body.highlighted).filter('description','=',req.body.description);
    await datastore.runQuery(query, function(err, entities) {
        entities.forEach(f => {
            f.highlighted = true;
            datastore.save(f);
        })
    });
    res.status(200).send("Highlighted issue");
})

app.patch('/issues/de-highlight', async (req, res) =>{
    const query = datastore.createQuery('issue').filter('type','=',req.body.type).filter('dateposted','=',req.body.dateposted).filter('dateofissue','=',req.body.dateofissue).filter('reviewed','=',req.body.reviewed).filter('highlighted','=',req.body.highlighted).filter('description','=',req.body.description);
    await datastore.runQuery(query, function(err, entities) {
        entities.forEach(f => {
            f.highlighted = false;
            datastore.save(f);
        })
    });
    res.status(200).send("Removed highlight from issue");
})

app.get('/issues/report', async (req, res) =>{
    const query = datastore.createQuery('issue');
    const result = await datastore.runQuery(query);
    const date = new Date();

    let report = {
        infrastructure: 0,
        safety: 0,
        public_health: 0,
        pollution: 0,
        noise_disturbing_the_peace: 0,
        other: 0,
        last24: [0,0,0,0,0,0],
        last24total:0,
        last7: [0,0,0,0,0,0],
        last7total: 0,
        numReviewed: 0,
        numPendingReview: 0
    }
    const data = result[0].map(r => {
        const difference = differenceInHours(
            date,
            new Date(r.dateofissue)
        )

        if(r.type === 'infrastructure'){
            if(difference < 168){
                report.last7[0]++
            } 
            if (difference < 24){
                report.last24[0]++
            }
            report.infrastructure++;
        } else if(r.type === 'safety'){
            if(difference < 168){
                report.last7[1]++
            } 
            if (difference < 24){
                report.last24[1]++
            }
            report.safety++;
        } else if(r.type === 'public_health'){
            if(difference < 168){
                report.last7[2]++
            } 
            if (difference < 24){
                report.last24[2]++
            }
            report.public_health++;
        } else if(r.type === 'pollution'){
            if(difference < 168){
                report.last7[3]++
            } 
            if (difference < 24){
                report.last24[3]++
            }
            report.pollution++;
        } else if(r.type === 'noise_disturbing_the_peace'){
            if(difference < 168){
                report.last7[4]++
            } 
            if (difference < 24){
                report.last24[4]++
            }
            report.noise_disturbing_the_peace++;
        } else {
            if(difference < 168){
                report.last7[5]++
            } 
            if (difference < 24){
                report.last24[5]++
            }
            report.other++;
        }
    })
    for(let i=0;i<report.last24.length;i++){
        report.last24total += report.last24[i];
    }
    for(let i=0;i<report.last7.length;i++){
        report.last7total += report.last7[i];
    }
    generateTextReport(report);
    res.status(200).send(report)
})


async function generateTextReport(report){
    textReport = `Infrastructure: ${report.infrastructure}, Safety: ${report.safety}, Public Health: ${report.public_health}, Pollution: ${report.pollution}, Noise/Disturbing the peace: ${report.noise_disturbing_the_peace},
     Other: ${report.other}. Issues in last 24 hours: ${report.last24total}- Infrastructure: ${report.last24[0]}, Safety: ${report.last24[1]}, Public Health: ${report.last24[2]}, Pollution: ${report.last24[3]}, Noise/Disturbing the peace: ${report.last24[4]},
     Other: ${report.last24[5]}. Issues in last 7 days: ${report.last7total}- Infrastructure: ${report.last7[0]}, Safety: ${report.last7[1]}, Public Health: ${report.last7[2]}, Pollution: ${report.last7[3]}, Noise/Disturbing the peace: ${report.last7[4]},
     Other: ${report.last7[5]}. Number reviewed: ${report.numReviewed}. Number pending review: ${report.numPendingReview}.`
     await writeFile("./logs/report.txt", textReport.toString());
}

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log(`Application Started on port ${PORT}`));